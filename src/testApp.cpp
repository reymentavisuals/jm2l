#include "testApp.h"

//--------------------------------------------------------------
// testApp.cpp
void testApp::setup()
{
	fillColor.set( 255, 0, 0 ); // rouge
	// dans setup()
	screenHeight = float( ofGetHeight() );
	player.loadMovie( "video.mp4" );
	player.play();
	isPaused = false;
}

//--------------------------------------------------------------
void testApp::update()
{
	// varier la couleur de remplissage
	fillColor.setHue( fillColor.getHue() + 1 );
	if ( fillColor.getHue() > 254 ) fillColor.setHue( 0 );
	// récupérer le temps en secondes depuis le lancement
    tempsEcoule = ofGetElapsedTimef(); 
    // sin renvoie une valeur entre -1 et 1
    valeur = sin( tempsEcoule * M_TWO_PI );
    // convertir la valeur pour être entre 0 et 255
    v = ofMap( valeur, -1, 1, 0, 255);
	// dans update()
	player.update();
}

//--------------------------------------------------------------
void testApp::draw()
{	
    // animer la couleur de fond
    ofBackground( v, 128, 0 );
	// ajout d'un cercle rempli
	ofSetColor( fillColor );
	ofFill();
	ofCircle( ofGetWidth() / 2, ofGetHeight() / 2, 200 );
	// dans draw()
	player.draw( 20, 20, 200, 200 );
}

//--------------------------------------------------------------
void testApp::keyPressed(int key)
{
	// dans keyPressed(int key)
	isPaused = !isPaused;
	player.setPaused( isPaused );
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y )
{
	// dans mouseMoved(int x, int y )
	float yPos = ( mouseY / screenHeight ) * 20.0;
	player.setSpeed( yPos );
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}
